class App {
    constructor() {
        this.machineData;
    }

    dataSync(data) {
        this.machineData = data;
    }

    machineAnalyze(id) {
        let machineWorkArr = [0, 0];
        const machineState = ["Простой", "Работа"];
        machineDataArray.forEach(elem => {
            if (elem[3] == id && this.isValidNumber(elem[6])) {
                elem[4] == machineState[0] ? machineWorkArr[0] += +elem[6] : machineWorkArr[1] += +elem[6];
            }
        });
        // alert(`В режиме работы: ${machineWorking}, в режиме простоя: ${machineStaying}`);
        this.machinePlot(machineWorkArr, machineState);
    };

    machinePlot(value, legend) {
        var data = [{
            values: value,
            labels: legend,
            type: 'pie'
        }];

        var layout = {
            height: 400,
            width: 500
        };

        Plotly.newPlot('myDiv', data, layout);
    }

    isValidNumber(numberToCheck) {
        return isNaN(+numberToCheck / 2) ? false : true;
    };

}