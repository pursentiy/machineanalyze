

machineAnalyze = (id) => {
    let machineWorkArr = [0, 0];
    const machineState = ["Простой", "Работа"];
    machineDataArray.forEach(elem => {
        if (elem[3] == id && isValidNumber(elem[6])) {
            elem[4] == machineState[0] ? machineWorkArr[0] += +elem[6] : machineWorkArr[1] += +elem[6];
        }
    });
    // alert(`В режиме работы: ${machineWorking}, в режиме простоя: ${machineStaying}`);
    machinePlot(machineWorkArr, machineState);
};

const machinePlot = (value, legend) => {
    var data = [{
        values: value,
        labels: legend,
        type: 'pie'
    }];

    var layout = {
        height: 400,
        width: 500
    };

    Plotly.newPlot('myDiv', data, layout);
}


const fileInput = document.getElementById('fileInput');
let machineDataArray = [];
fileInput.addEventListener('change', function (e) {
    var file = fileInput.files[0];
    Papa.parse(file, {
        complete: function (results) {
            machineDataArray = results.data;
            machineDataArray.shift();
            let test = analyzeDate(dateParse(machineDataArray));
            console.log(test);
        }
    });
});


const machineID = document.getElementById('machineID');
machineID.onchange = function () {
    machineAnalyze(machineID.value);
};

let uniqDates = [[], []];

const dateParse = (dates) => {
    let datesToCheck = [[], []];
    dates.forEach((elem) => {
        let rowDate = elem[0].split(" ");
        let monthAndDay = rowDate[0].split("-");
        if (isValidNumber(monthAndDay[1]) && isValidNumber(monthAndDay[2])) {
            return (datesToCheck[0].push(monthAndDay[1]) && datesToCheck[1].push(monthAndDay[2]));
        }
    })
    return datesToCheck;
}

const analyzeDate = (dataToAnalyze) => {
    return ([_.uniq(dataToAnalyze[0]), _.uniq(dataToAnalyze[1])]);
}

const updateSelect = (unicMonths) => {
    const month = document.getElementById("month");
    let monthNumber = 0;
    monthNumber = this.convertingMonth(unicMonths)[0][1];
    unicMonths.forEach(monthInsert => {
        month.options[month.options.length] = new Option(
            monthNumber.toString(),
            monthInsert[1]
        );
    });
}





const isValidNumber = (numberToCheck) => {
    return isNaN(+numberToCheck / 2) ? false : true;
};


